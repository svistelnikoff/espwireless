###############################################################################
# Makefile for the project
###############################################################################

# Autodetect environment
SHELL   = sh
ifneq ($(or $(COMSPEC), $(ComSpec)),)
$(info COMSPEC detected $(COMSPEC) $(ComSpec))
ifeq ($(findstring cygdrive,$(shell set)),)
SHELL:=$(or $(COMSPEC),$(ComSpec))
SHELL_IS_WIN32=1
REMOVE_CMD=del /q
else
$(info cygwin detected)
#override user-setting since cygwin has rm
REMOVE_CMD:=rm
endif
else
#most probaly a Unix/Linux/BSD system which should have rm
REMOVE_CMD:=rm -R -f
endif
$(info SHELL is $(SHELL), REMOVE_CMD is $(REMOVE_CMD))

REMOVE = $(REMOVE_CMD)

ifeq ($(OS),Windows_NT)
  # Windows detected
  UNAME := Windows
  
  # Default SMING_HOME. Can be overriden.
  SMING_HOME ?= c:\tools\Sming\Sming

  # Default ESP_HOME. Can be overriden.
  ESP_HOME ?= c:\Espressif

  # Making proper path adjustments - replace back slashes, remove colon and add forward slash.
  SMING_HOME := $(subst \,/,$(addprefix /,$(subst :,,$(SMING_HOME))))
  ESP_HOME := $(subst \,/,$(addprefix /,$(subst :,,$(ESP_HOME))))
  SDK_HOME ?=$(ESP_HOME)\esp_iot_sdk_v1.5.2
  ## COM port parameter is reqruied to flash firmware correctly.
  ## Windows: 
  # COM_PORT = COM3
else
  UNAME := $(shell uname -s)
  ifeq ($(UNAME),Darwin)
      # MacOS Detected
      UNAME := MacOS

      # Default SMING_HOME. Can be overriden.
      SMING_HOME ?= /opt/sming/Sming

      # Default ESP_HOME. Can be overriden.
      ESP_HOME ?= /opt/esp-open-sdk
      SDK_HOME ?=$(ESP_HOME)/esp_iot_sdk_v1.5.2
  endif
  ifeq ($(UNAME),Linux)
      # Linux Detected
      UNAME := Linux

      # Default SMING_HOME. Can be overriden.
      SMING_HOME ?= /home/vvs/Sming/Sming

      # Default ESP_HOME. Can be overriden.
      ESP_HOME ?= /opt/esp-open-sdk
      SDK_HOME ?=$(ESP_HOME)/esp_iot_sdk_v1.5.2
      
      ESPTOOL = $(ESP_HOME)/esptool/esptool.py
	  KILL_TERM  = pkill -9 -f "$(COM_PORT) $(COM_SPEED_SERIAL)" || exit 0
      
  endif
  ifeq ($(UNAME),FreeBSD)
      # Freebsd Detected
      UNAME := FreeBSD

      # Default SMING_HOME. Can be overriden.
      SMING_HOME ?= /usr/local/esp8266/Sming/Sming

      # Default ESP_HOME. Can be overriden.
      ESP_HOME ?= /usr/local/esp8266/esp-open-sdk
      SDK_HOME ?=$(ESP_HOME)/esp_iot_sdk_v1.5.2
  endif
  # MacOS / Linux:
  COM_PORT = /dev/ttyUSB0
endif

COM_SPEED = 115200

ifeq ($(SPI_SPEED), 26)
	flashimageoptions = -ff 26m
else ifeq ($(SPI_SPEED), 20)
	flashimageoptions = -ff 20m
else ifeq ($(SPI_SPEED), 80)
	flashimageoptions = -ff 80m
else
	flashimageoptions = -ff 40m
endif

## Configure flash parameters (for ESP12-E and other new boards):
# SPI_MODE = dio
SPI_MODE = qio

ifeq ($(SPI_MODE), qout)
	flashimageoptions += -fm qout
else ifeq ($(SPI_MODE), dio)
	flashimageoptions += -fm dio
else ifeq ($(SPI_MODE), dout)
	flashimageoptions += -fm dout
else
	flashimageoptions += -fm qio
endif

# flash larger than 1024KB only use 1024KB to storage user1.bin and user2.bin
ifeq ($(SPI_SIZE), 256K)
	flashimageoptions += -fs 2m
	SPIFF_SIZE ?= 131072  #128K
else ifeq ($(SPI_SIZE), 1M)
	flashimageoptions += -fs 8m
	SPIFF_SIZE ?= 524288  #512K
else ifeq ($(SPI_SIZE), 2M)
	flashimageoptions += -fs 16m
	SPIFF_SIZE ?= 524288  #512K
else ifeq ($(SPI_SIZE), 4M)
	flashimageoptions += -fs 32m
	SPIFF_SIZE ?= 524288  #512K
else
	flashimageoptions += -fs 4m
	SPIFF_SIZE ?= 196608  #192K
endif

# Path to spiffy
SPIFFY ?= $(SMING_HOME)/spiffy/spiffy

#ESPTOOL2 config to generate rBootLESS images
IMAGE_MAIN	?= 0x00000.bin
IMAGE_SDK	?= 0x09000.bin
# esptool2 path
ESPTOOL2 ?= $(ESP_HOME)/esptool2/esptool2
# esptool2 parameters for rBootLESS images
ESPTOOL2_SECTS		?= .text .data .rodata
ESPTOOL2_MAIN_ARGS	?= -quiet -bin -boot0
ESPTOOL2_SDK_ARGS	?= -quiet -lib

SPIFF_BIN_OUT ?= spiff_rom
SPIFF_BIN_OUT := $(FW_BASE)/$(SPIFF_BIN_OUT).bin
# Com port speed
COM_SPEED = 115200

export ESP_HOME
export SDK_HOME
export SMING_HOME
#export COMPILE := gcc
export PATH := $(ESP_HOME)/xtensa-lx106-elf/bin:$(PATH)

TOOLS_LOC = $(ESP_HOME)
TOOL_LOC =  $(TOOLS_LOC)/xtensa-lx106-elf
TCHAIN_LOC = $(TOOL_LOC)/bin
# Toolchain prefix (arm-elf- -> arm-elf-gcc.exe)
#TCHAIN_PREFIX = arm-none-eabi-
#TCHAIN_PREFIX = arm-eabi-
#TCHAIN_PREFIX = arm-elf-
TCHAIN_PREFIX = xtensa-lx106-elf-

OUTDIR 		= output
BUILD_BASE	= $(OUTDIR)/build
FW_BASE		= $(OUTDIR)/firmware
SPIFF_DIR 	= $(OUTDIR)/files

# name for the target project
TARGET		= ESPWireless

# which modules (subdirectories) of the project to include in compiling
#EXTRA_SRCDIR = system system/helpers Wiring SmingCore appinit $(filter %/, $(wildcard SmingCore/*/)) $(filter %/, $(wildcard Services/*/)) $(filter %/, $(wildcard Libraries/*/))
EXTRA_INCDIR = $(SMING_HOME) $(SMING_HOME)/Wiring $(SMING_HOME)/rboot $(SMING_HOME)/rboot/appcode $(SMING_HOME)/SmingCore $(SMING_HOME)/system/include


# user or other libraries used in this project
#USER_LIBDIR = $(SMING_HOME)/compiler/lib
#USER_LIBS = microc microgcc sming

# compiler flags using during compilation of source files. Add '-pg' for debugging
CFLAGS = -Wpointer-arith -Wundef -Werror -Wl,-EL -nostdlib -mlongcalls -mtext-section-literals
CFLAGS += -finline-functions -fdata-sections -ffunction-sections -D__ets__ -DICACHE_FLASH -DARDUINO=106

ifeq ($(ENABLE_GDB), 1)
	CFLAGS 		 += -Og -ggdb -DGDBSTUB_FREERTOS=0 -DENABLE_GDB=1
	MODULES		 += gdbstub
	EXTRA_INCDIR += gdbstub
else
	CFLAGS += -Os -g
endif

CXXFLAGS = $(CFLAGS) -fno-rtti -fno-exceptions -std=c++11 -felide-constructors

# linker script used for the above linkier step
LD_SCRIPT = eagle.app.v6.ld

# linker flags used to generate the main object file
LDFLAGS	= -T$(SDK_LDDIR)/$(LD_SCRIPT) -nostdlib -u call_user_start -Wl,-static -Wl,--gc-sections 
LDFLAGS += -Wl,-Map=$(BUILD_BASE)/$(TARGET).map

# libraries used in this project, mainly provided by the SDK
LIBS = hal phy pp net80211 lwip wpa main crypto pwm json
#LIBS += $(USER_LIBS)
LIBS += c gcc

# various paths from the SDK used in this project
SDK_LIBDIR	= $(SDK_HOME)/lib
SDK_LDDIR	= $(SDK_HOME)/ld
SDK_INCDIR	= $(SDK_HOME)/include
SDK_INCDIR += $(SDK_HOME)/include/json

# select which tools to use as compiler, librarian and linker
AS		:= $(TCHAIN_LOC)/$(TCHAIN_PREFIX)gcc
CC		:= $(TCHAIN_LOC)/$(TCHAIN_PREFIX)gcc
CXX		:= $(TCHAIN_LOC)/$(TCHAIN_PREFIX)g++
AR		:= $(TCHAIN_LOC)/$(TCHAIN_PREFIX)ar
LD		:= $(TCHAIN_LOC)/$(TCHAIN_PREFIX)gcc
OBJCOPY := $(TCHAIN_LOC)/$(TCHAIN_PREFIX)objcopy
OBJDUMP := $(TCHAIN_LOC)/$(TCHAIN_PREFIX)objdump

MEMORYINFO = $(OBJDUMP) -h -j .data -j .rodata -j .bss -j .text -j .irom0.text
LISTING = $(OBJDUMP) -h -S -C -r
SPIFFY = $(SMING_HOME)/spiffy/spiffy

SRCDIR = src driver

#getting source files from 
AS_SRC		:= $(foreach sdir,$(SRCDIR),$(wildcard $(sdir)/*.s))
ifeq ($(ENABLE_GDB), 1)
	AS_SRC += $(foreach sdir,$(SRCDIR),$(wildcard $(sdir)/*.S)) 
endif
C_SRC		:= $(foreach sdir,$(SRCDIR),$(wildcard $(sdir)/*.c))
CXX_SRC		:= $(foreach sdir,$(SRCDIR),$(wildcard $(sdir)/*.cpp))

#retrieving object filenames from source files
AS_OBJS		:= $(patsubst %.s,$(BUILD_BASE)/%.o,$(AS_SRC))
ifeq ($(ENABLE_GDB), 1)
	AS_OBJS		+= $(patsubst %.S,$(BUILD_BASE)/%.o,$(notdir $(AS_SRC)))
endif
C_OBJS		:= $(patsubst %.c,$(BUILD_BASE)/%.o,$(notdir $(C_SRC)))
CXX_OBJS		:= $(patsubst %.cpp,$(BUILD_BASE)/%.o,$(notdir $(CXX_SRC)))
OBJS			:= $(AS_OBJS) $(C_OBJS) $(CXX_OBJS)

#defining libraries and output files
LIBS		:= $(addprefix -l,$(LIBS))
TARGET_APP	:= $(addprefix $(BUILD_BASE)/,$(TARGET).a)
TARGET_OUT	:= $(addprefix $(BUILD_BASE)/,$(TARGET).out)

INCLUDES	= $(addprefix -I,$(SRCDIR))
INCLUDES	+= $(addprefix -I,$(SDK_INCDIR))
INCLUDES	+= $(addprefix -I,$(EXTRA_INCDIR))
#INCLUDES	+= -I./include

vpath %.c $(SRCDIR)
vpath %.cpp $(SRCDIR)
vpath %.s $(SRCDIR)
ifeq ($(ENABLE_GDB), 1)
	vpath %.S $(SRCDIR)
endif

# Compile: create object files from C++ source files.
define compile-sources
# Compiling C sources
$(BUILD_BASE)/%.o: $(1)/%.c $(BUILDONCHANGE)
	@echo "Compiling C - $$<"
	$(Q) $(CC) $(INCLUDES) $(CFLAGS) -c $$< -o $$@

# Compiling C++ sources
$(BUILD_BASE)/%.o: $(1)/%.cpp $(BUILDONCHANGE)
	@echo "Compiling C++ - $$<"
	$(Q) $(CXX) $(INCLUDES) $(CXXFLAGS)  -c $$< -o $$@
endef
$(foreach srcdir, $(SRCDIR), $(eval $(call compile-sources, $(srcdir)))) 

.PHONY: all gccversion sources objects includes spiff_clean spiff_update flash flashinit rebuild createdirs

all: gccversion createdirs $(TARGET_OUT) spiff_update

# Display compiler version information.
gccversion: 
	$(CC) --version

sources:
	@echo $(C_SRC) $(CXX_SRC)

objects:
	@echo $(OBJS)
	
includes:
	@echo $(INCLUDES)

$(TARGET_APP): $(OBJS)
	@echo "AR $@"
	$(Q) $(AR) cru $@ $^

$(TARGET_OUT): $(TARGET_APP)
	@echo "LD $@"	
	$(LD) -L$(USER_LIBDIR) -L$(SDK_LIBDIR) $(LDFLAGS) -Wl,--start-group $(LIBS) $(TARGET_APP) -Wl,--end-group -o $@

	@echo ""	
	@echo "# Generating listing file $(BUILD_BASE)/$(TARGET).lss"
	@echo ""	
	$(LISTING) $@ > $(BUILD_BASE)/$(TARGET).lss
	@echo "# Memory / Section info:"	
	@echo "------------------------------------------------------------------------------"
	$(MEMORYINFO) $@
	@echo "------------------------------------------------------------------------------"
	@echo "# Generating image in folder $(FW_BASE)"
	$(ESPTOOL) elf2image $@ $(flashimageoptions) -o $(FW_BASE)/
#	@$(ESPTOOL2) $(ESPTOOL2_MAIN_ARGS) $@ $(FW_BASE)/$(IMAGE_MAIN) $(ESPTOOL2_SECTS)
#	@$(ESPTOOL2) $(ESPTOOL2_SDK_ARGS) $@ $(FW_BASE)/$(IMAGE_SDK)
	@echo ""
	
SPIFF_BIN_NAME = spiff_rom
SPIFF_BIN_OUT = $(FW_BASE)/$(SPIFF_BIN_NAME).bin
DISABLE_SPIFFS = 1

spiff_clean: 
	@echo "Cleaning $(SPIFF_BIN_OUT)"
	@$(REMOVE) $(SPIFF_BIN_OUT)

ifeq ($(DISABLE_SPIFFS), 1)
	spiff_update:
else
	spiff_update: spiff_clean $(SPIFF_BIN_OUT)
endif

$(SPIFF_BIN_OUT):
ifeq ($(DISABLE_SPIFFS), 1)
	@echo "(!) Spiffs support disabled. Remove 'DISABLE_SPIFFS' make argument to enable spiffs."
else
	# Generating spiffs_bin
	@echo "Checking for spiffs files"
	$(Q) if [ -d "$(SPIFF_DIR)" ]; then \
		echo "$(SPIFF_DIR) directory exists. Creating $(SPIFF_BIN_OUT)"; \
		ls \
		$(SPIFFY) $(SPIFF_SIZE) $(SPIFF_DIR) $(SPIFF_BIN_OUT); \
	else \
		echo "No files found in ./$(SPIFF_DIR)."; \
		echo "Creating empty $(SPIFF_BIN_OUT)"; \
		$(SPIFFY) $(SPIFF_SIZE) dummy.dir $(SPIFF_BIN_OUT); \
	fi
	@echo "$(SPIFF_BIN_OUT)---------->$(SPIFF_START_OFFSET)"
endif

flash: all
ifeq ($(DISABLE_SPIFFS), 1)
	$(ESPTOOL) -p $(COM_PORT) -b $(COM_SPEED) write_flash $(flashimageoptions) 0x00000 $(FW_BASE)/0x00000.bin 0x40000 $(FW_BASE)/0x40000.bin
else
	$(ESPTOOL) -p $(COM_PORT) -b $(COM_SPEED) write_flash $(flashimageoptions) 0x00000 $(FW_BASE)/0x00000.bin 0x40000 $(FW_BASE)/0x40000.bin $(SPIFF_START_OFFSET) $(SPIFF_BIN_OUT)
endif

flashinit:
	@echo "Flash init data default and blank data."
	$(ESPTOOL) -p $(COM_PORT) -b $(COM_SPEED) write_flash $(flashimageoptions) 0x7c000 $(SDK_BASE)/bin/esp_init_data_default.bin 0x7e000 $(SDK_BASE)/bin/blank.bin 0x4B000 $(SMING_HOME)/compiler/data/blankfs.bin
rebuild: clean all

clean:
	$(REMOVE) $(BUILD_BASE)/*
	$(REMOVE) $(BUILD_BASE)/dep/*
	$(REMOVE) $(FW_BASE)/*

# Create output directories.
ifdef SHELL_IS_WIN32
createdirs:
	-@md $(BUILD_BASE) >NUL 2>&1 || echo "" >NUL
	-@md $(BUILD_BASE)/dep >NUL 2>&1 || echo "" >NUL
	-@md $(FW_BASE) >NUL 2>&1 || echo "" >NUL
else
createdirs:
	-@mkdir -p $(BUILD_BASE) 2>/dev/null || echo "" >/dev/null
	-@mkdir -p $(BUILD_BASE)/dep 2>/dev/null || echo "" >/dev/null
	-@mkdir -p $(FW_BASE) 2>/dev/null || echo "" >/dev/null
endif

#discovery target for Eclipse parser
# Eclipse CDT GCC build-in compiler settings -> Providers
# Command to get compiler specs:
# make extension="${EXT}" specs_file="${INPUTS}" discovery
# all flags unchecked except: Allocate console in Console View 
.PHONY: discovery

discovery:
ifeq ($(extension),C)
	@echo "-- discovery for $(CXX)"
	$(CXX) $(INCLUDES) $(CXXFLAGS) -E -P -v -dD "$(specs_file)"
else
	@echo "-- discovery for $(CC)"
	$(CC) $(INCLUDES) $(CFLAGS) -E -P -v -dD "$(specs_file)"
endif
	$(REMOVE) spec.d
	
