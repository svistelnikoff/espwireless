/*
 * user_main.c
 *
 *  Created on: Mar 17, 2016
 *      Author: vvs
 */

#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "os_type.h"
#include "user_interface.h"

#include "user_wireless.h"
#include "user_udp.h"
#include "user_tcp.h"
#include "light.h"
#include "user_settings.h"

#define LED_PIN_NUM			4

//=============================================================================================================================================================================================================================================

volatile os_timer_t MainTimer;
volatile os_timer_t BlinkTimer;
volatile uint32_t ticks;

void MainTimerCallback(void *arg);
void BlinkTimerCallback(void);

//=============================================================================================================================================================================================================================================

void  user_rf_pre_init(){

}

//=============================================================================================================================================================================================================================================

void user_init(){
	gpio_init();
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);
	GPIO_OUTPUT_SET(LED_PIN_NUM, 1);


	UART_SetBaudrate(0, 115200);
	UART_ResetFifo(0);

	ticks = 0;

	InitUserSettings();

	InitUDPTask();
	InitTCPTask();

	light_init();

	os_timer_setfn(&MainTimer, MainTimerCallback, NULL);
	os_timer_setfn(&BlinkTimer, BlinkTimerCallback, NULL);
	os_timer_arm(&BlinkTimer, 1000, TRUE);
	user_set_station_config();
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
MainTimerCallback(void *arg){
	os_timer_disarm(&MainTimer);
	GPIO_OUTPUT_SET(LED_PIN_NUM, 0);
	system_os_post(UDP_TASK_PRIORITY, UDP_SIGNAL_OTHER, ticks);
	system_os_post(TCP_TASK_PRIORITY, TCP_SIGNAL_OTHER, ticks);
}

//=============================================================================================================================================================================================================================================

void BlinkTimerCallback(void){
	os_timer_arm(&MainTimer,20,0);
	GPIO_OUTPUT_SET(LED_PIN_NUM, 1);
	ticks++;
}

//=============================================================================================================================================================================================================================================


