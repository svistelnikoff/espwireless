/*
 * user_udp.h
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#ifndef SRC_USER_UDP_H_
#define SRC_USER_UDP_H_

#define UDP_TASK_PRIORITY		USER_TASK_PRIO_0
#define UDP_QUEUE_LENGTH		5
#define UDP_SIGNAL_PRINT		0
#define UDP_SIGNAL_OTHER		1

//=============================================================================================================================================================================================================================================

void InitUDPTask(void);
void UDPTask(os_event_t *pevent);
void init_udp_connection(void);
void udpSendData(void);
void UDPClientSentCallback(void *arg);
void UDPClientReceivedCallback(void *arg, char *pdata, uint16_t length);

//=============================================================================================================================================================================================================================================

#endif /* SRC_USER_UDP_H_ */
