/*
 * user_udp.c
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "espconn.h"

#include "user_wireless.h"
#include "user_settings.h"
#include "user_udp.h"

extern volatile uint32_t ticks;

//=============================================================================================================================================================================================================================================

static struct espconn *pUDPConnection = NULL;
static os_event_t *pUDPQueue = NULL;

static char UDPData[100];

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
InitUDPTask(void){
	pUDPQueue = (os_event_t*) os_malloc(UDP_QUEUE_LENGTH*sizeof(os_event_t));
	system_os_task(UDPTask, UDP_TASK_PRIORITY, pUDPQueue, UDP_QUEUE_LENGTH);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
UDPTask(os_event_t *pevent){
	if(wifi_station_get_connect_status() != STATION_GOT_IP) return;
	switch (pevent->sig) {
		case UDP_SIGNAL_PRINT:
			//os_printf("UDP_SIGNAL_PRINT received ! Ticks count: %lu\r\n", (uint32_t)pevent->par);
			os_printf("Free heap size: %lu\r\n", (uint32_t)system_get_free_heap_size());
			break;
		case UDP_SIGNAL_OTHER:
			//os_printf("UDP_SIGNAL_OTHER received ! Ticks count: %lu\r\n", (uint32_t)pevent->par);
			udpSendData();
			break;
		default:
			break;
	}
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
udpSendData(){
	int8_t result;

	if(pUDPConnection == NULL){
		pUDPConnection = (struct espconn*) os_zalloc(sizeof(struct espconn));
		pUDPConnection->type = ESPCONN_UDP;
		pUDPConnection->state = ESPCONN_NONE;
		pUDPConnection->proto.udp = (esp_udp*) os_zalloc(sizeof(esp_udp));
		pUDPConnection->proto.udp->local_port = RemoteUDPClientPort;
		pUDPConnection->proto.udp->remote_port = RemoteUDPClientPort;
		os_memcpy(pUDPConnection->proto.udp->remote_ip,RemoteIP, 4);
		result = espconn_create(pUDPConnection);
		espconn_regist_recvcb(pUDPConnection, UDPClientReceivedCallback);
		espconn_regist_sentcb(pUDPConnection, UDPClientSentCallback);
		if(result != ESPCONN_OK || result != ESPCONN_ISCONN){
			os_printf("espconn_create(pUDPConnection) result: %d\r\n", result);
		}
	}else{
		os_sprintf(UDPData, "Hello from UDP ! Ticks: %lu. My name is %s\r\n", ticks, UserSettings.Name);
		result = os_strlen(UDPData);
		espconn_send(pUDPConnection, (uint8_t*)UDPData, result+1);
	}
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
UDPClientSentCallback(void *arg){

}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
UDPClientReceivedCallback(void *arg, char *pdata, uint16_t length){
	if(length > 21){
		length = 21;
		pdata[20] = '\0';
	}
	os_printf("In UDPClientReceivedCallback...Length: %u,Data: %s", length, pdata);
	if(pdata[length-1] == '\n') pdata[length-1] = '\0';
	os_strcpy(UserSettings.Name, pdata);
	UserSettings.CRC = CalculateUserSettingsCRC();
	UserSettingsSave();
}

//=============================================================================================================================================================================================================================================


