/*
 * usr_settings.h
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#ifndef SRC_USER_SETTINGS_H_
#define SRC_USER_SETTINGS_H_

//=============================================================================================================================================================================================================================================

#define USER_SETTINGS_SECTOR_NUMBER			(uint16_t)0x3C

#define FUSERSETTINGS_EMTPY					0UL
#define FUSERSETTINGS_FAILURE				(1UL<<31)

//=============================================================================================================================================================================================================================================

typedef struct{
	char Name[21];
	uint8_t light_intensity;
	uint32_t CRC;
}TSettings;

extern TSettings UserSettings;

//=============================================================================================================================================================================================================================================

void InitUserSettings(void);
uint32_t UserSettingsLoad(void);
void UserSettingsLoadDefault(void);
uint32_t UserSettingsSave(void);
uint32_t CalculateUserSettingsCRC(void);
uint32_t CheckUserSettings(void);
void UserSettingCheckTimerCallback(void *arg);

uint32_t SaveLightIntensity(uint8_t value);

//=============================================================================================================================================================================================================================================

#endif /* SRC_USER_SETTINGS_H_ */
