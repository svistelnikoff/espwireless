/*
 * user_wireless.c
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "user_interface.h"
#include "user_config.h"

#include "user_wireless.h"

const char ssid[32] = "tomato";
const char password[64] = "1234554321";
const char sta_mac[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 0xab};

const uint8_t RemoteIP[4] = REMOTE_IP;

const uint16_t LocalTCPServerPort = 80;
const uint16_t RemoteTCPClientPort = 12346;

const uint16_t RemoteUDPClientPort = 12345;

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
user_set_station_config(void)
{
	static struct station_config stationConf;

	wifi_set_opmode(1);								//set operation mode - station
	wifi_set_event_handler_cb(WiFiEventCallBack);
	//wifi_set_macaddr(1, sofap_mac);				//set sfotAP MAC address
	wifi_set_macaddr(0, (char*)sta_mac);				//set station MAC address

	stationConf.bssid_set = 0; 						//need not check MAC address of AP
	os_memcpy(&stationConf.ssid, ssid, 32);
	os_memcpy(&stationConf.password, password, 64);
	wifi_station_set_config(&stationConf);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
WiFiEventCallBack(System_Event_t *evt){
	os_printf("event %x\n", evt->event);
	switch (evt->event) {
		case EVENT_STAMODE_CONNECTED:
			os_printf("connect to ssid %s, channel %d\n",
			evt->event_info.connected.ssid,
			evt->event_info.connected.channel);
			break;
		case EVENT_STAMODE_DISCONNECTED:
			os_printf("disconnect from ssid %s, reason %d\n",
			evt->event_info.disconnected.ssid,
			evt->event_info.disconnected.reason);
			break;
		case EVENT_STAMODE_AUTHMODE_CHANGE:
			os_printf("mode: %d -> %d\n",
			evt->event_info.auth_change.old_mode,
			evt->event_info.auth_change.new_mode);
			break;
		case EVENT_STAMODE_GOT_IP:
			os_printf("ip:" IPSTR ",mask:" IPSTR ",gw:" IPSTR, IP2STR(&evt->event_info.got_ip.ip), IP2STR(&evt->event_info.got_ip.mask),IP2STR(&evt->event_info.got_ip.gw));
			os_printf("\n");
			break;
		case EVENT_SOFTAPMODE_STACONNECTED:
			os_printf("station: " MACSTR "join, AID = %d\n", MAC2STR(evt->event_info.sta_connected.mac), evt->event_info.sta_connected.aid);
			break;
		case EVENT_SOFTAPMODE_STADISCONNECTED:
			os_printf("station: " MACSTR "leave, AID = %d\n", MAC2STR(evt->event_info.sta_disconnected.mac), evt->event_info.sta_disconnected.aid);
			break;
		default:
			break;
	}
}

//=============================================================================================================================================================================================================================================
