/*
 * user_tcp.h
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#ifndef SRC_USER_TCP_H_
#define SRC_USER_TCP_H_

//=============================================================================================================================================================================================================================================

#define TCP_TASK_PRIORITY			USER_TASK_PRIO_1
#define TCP_QUEUE_LENGTH			5
#define TCP_SIGNAL_PRINT			0
#define TCP_SIGNAL_OTHER			1
#define TCP_SIGNAL_SERVER_PRINT		2

#define FTCP_CLIENT_EMPTY						0
#define FTCP_CLIENT_DISCONNECT					(1UL<<0)
#define FTCP_CLIENT_DELETE_CONNECTION			(1UL<<1)

#define FTCP_SERVER_EMPTY						0
#define FTCP_SERVER_STARTED						(1UL<<0)
#define FTCP_SERVER_DISCONNECT					(1UL<<1)
#define FTCP_SERVER_DELETE						(1UL<<2)


//=============================================================================================================================================================================================================================================


void InitTCPTask(void);
void TCPTask(os_event_t *pevent);
void TCPClientSend(void);
int8_t TCPClientInit(void);
void TCPClientConnectCallback(void *arg);
void TCPClientDisconnectCallback(void *arg);
void TCPClientSentCallback(void *arg);
void TCPClientReceiveCallback(void *arg, char *pdata, uint16_t length);
void TCPClientReconnectCallback(void *arg, int8_t error);

int8_t TCPServerInit(void);
void TCPServerStop(void);
void TCPServerConnectCallback(void *arg);
void TCPServerDisconnectCallback(void *arg);
void TCPServerSentCallback(void *arg);
void TCPServerReceiveCallback(void *arg, char *pdata, uint16_t length);
void TCPServerReconnectCallback(void *arg, int8_t error);

//=============================================================================================================================================================================================================================================

#endif /* SRC_USER_TCP_H_ */
