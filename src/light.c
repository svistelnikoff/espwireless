/*
 * light.c
 *
 *  Created on: Jun 4, 2016
 *      Author: vvs
 */

#include "os_type.h"
#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "pwm.h"

#include "user_settings.h"
#include "light.h"

#define PWM_0_OUT_IO_MUX 	PERIPHS_IO_MUX_MTDI_U
#define PWM_0_OUT_IO_NUM 	12
#define PWM_0_OUT_IO_FUNC 	FUNC_GPIO12
#define PWM_CHANNELS		1
#define PWM_PERIOD			2000

void ICACHE_FLASH_ATTR
LightTimerCallback(void *arg);

static uint32_t pwm_duty_value = 0;
static uint8_t light_intensity_current;
static uint8_t light_intensity_new;
static uint8_t light_is_updating = FALSE;

os_timer_t LightTimer;

static uint32_t pwm_io_info[PWM_CHANNELS][3] = {
		{PWM_0_OUT_IO_MUX, PWM_0_OUT_IO_FUNC, PWM_0_OUT_IO_NUM}
};


void light_init(void){
	light_intensity_current = 0;
	light_intensity_new = UserSettings.light_intensity;

	os_timer_setfn(&LightTimer, LightTimerCallback, NULL);

	pwm_init(PWM_PERIOD, &pwm_duty_value,PWM_CHANNELS,pwm_io_info);
	light_set_intencity(light_intensity_new);
}

void light_set_intencity(uint8_t value){
	if (value > 100) value = 100;
	light_intensity_new = value;
	if (light_intensity_new != UserSettings.light_intensity) SaveLightIntensity(value);
	if (light_intensity_new != light_intensity_current){
		if (light_is_updating) return;
		os_timer_arm(&LightTimer, 5, 0);
		light_is_updating = TRUE;
	}
}

void ICACHE_FLASH_ATTR
LightTimerCallback(void *arg){
	if(light_intensity_current == light_intensity_new){
		os_timer_disarm(&LightTimer);
		light_is_updating = FALSE;
		return;
	}
	if (light_intensity_current < light_intensity_new){
		light_intensity_current++;
	}else{
		light_intensity_current--;
	}
	pwm_set_duty(light_intensity_current*PWM_PERIOD*10/45, 0);
	pwm_start();
	os_timer_arm(&LightTimer, 50, 0);
}
