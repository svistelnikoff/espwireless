/*
 * light.h
 *
 *  Created on: Jun 4, 2016
 *      Author: vvs
 */

#ifndef SRC_LIGHT_H_
#define SRC_LIGHT_H_

void light_init(void);
void light_set_intencity(uint8_t value);

#endif /* SRC_LIGHT_H_ */
