/*
 * user_wireless.h
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#ifndef SRC_USER_WIRELESS_H_
#define SRC_USER_WIRELESS_H_

#define REMOTE_IP				{192,168,1,100}

extern const uint8_t RemoteIP[4];

extern const uint16_t LocalTCPServerPort;
extern const uint16_t RemoteTCPClientPort;

extern const uint16_t RemoteUDPClientPort;

//=============================================================================================================================================================================================================================================

void user_set_station_config(void);
void WiFiEventCallBack(System_Event_t *evt);

//=============================================================================================================================================================================================================================================


#endif /* SRC_USER_WIRELESS_H_ */
