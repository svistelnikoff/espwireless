/*
 * user_settings.c
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#include "osapi.h"
#include "user_interface.h"
#include "spi_flash.h"

#include "user_settings.h"

//=============================================================================================================================================================================================================================================

uint32_t FUserSettings;
TSettings UserSettings __attribute__((aligned(4)));
static os_timer_t UserSettingsCheckTimer;

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
InitUserSettings(){
	FUserSettings = FUSERSETTINGS_FAILURE;
	if(UserSettingsLoad() == 1) UserSettingsLoadDefault();
	else FUserSettings &=~FUSERSETTINGS_FAILURE;

	os_timer_setfn(&UserSettingsCheckTimer, UserSettingCheckTimerCallback, NULL);
	os_timer_arm(&UserSettingsCheckTimer, 300, 1);
}

//=============================================================================================================================================================================================================================================

void UserSettingCheckTimerCallback(void *arg){
	CheckUserSettings();
}

//=============================================================================================================================================================================================================================================

uint32_t ICACHE_FLASH_ATTR
UserSettingsLoad(){
	spi_flash_read(USER_SETTINGS_SECTOR_NUMBER*0x1000, (uint32_t*)&UserSettings, sizeof(UserSettings));
	return (CheckUserSettings());
}

//=============================================================================================================================================================================================================================================

uint32_t ICACHE_FLASH_ATTR
CalculateUserSettingsCRC(){
	uint32_t CRC = 0x55555555;
	uint8_t *pUserSettings = (uint8_t*)&UserSettings;
	while(pUserSettings < ((uint8_t*)&UserSettings + sizeof(TSettings) - 4)){
		//os_printf("%d ", *pUserSettings);
		CRC += *pUserSettings++;
	}
	//os_printf("\r\n CRC = %lu\r\n", CRC);
	return (CRC);
}

//=============================================================================================================================================================================================================================================

uint32_t ICACHE_FLASH_ATTR
CheckUserSettings(){
	if(CalculateUserSettingsCRC() == UserSettings.CRC){
		//os_printf("UserSettings OK !\r\n");
		FUserSettings &=~FUSERSETTINGS_FAILURE;
		return (0);
	} else{
		os_printf("UserSettings FAILURE !\r\n");
		FUserSettings |= FUSERSETTINGS_FAILURE;
		return (1);
	}
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
UserSettingsLoadDefault(){
	os_memcpy(UserSettings.Name, "Vladimir", 9);
	UserSettings.light_intensity = 100;
	UserSettings.CRC = CalculateUserSettingsCRC();
	UserSettingsSave();
}

//=============================================================================================================================================================================================================================================

uint32_t ICACHE_FLASH_ATTR
UserSettingsSave(){
	SpiFlashOpResult result;
	spi_flash_erase_sector(USER_SETTINGS_SECTOR_NUMBER);
	result = spi_flash_write(USER_SETTINGS_SECTOR_NUMBER*0x1000, (uint32_t*)&UserSettings, sizeof(UserSettings));
	os_printf("\r\nIn UserSettingsSave()...Result: %2lu\r\n\r\n", result);
	return (result);
}

//=============================================================================================================================================================================================================================================

uint32_t ICACHE_FLASH_ATTR
SaveLightIntensity(uint8_t value){
	UserSettings.light_intensity = value;
	UserSettings.CRC = CalculateUserSettingsCRC();
	return (UserSettingsSave());
}
