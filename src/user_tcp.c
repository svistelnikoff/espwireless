/*
 * user_tcp.c
 *
 *  Created on: Mar 19, 2016
 *      Author: vvs
 */

#include "osapi.h"
#include "mem.h"
#include "user_interface.h"
#include "espconn.h"
#include "user_wireless.h"
#include "json.h"
#include "jsonparse.h"
#include "jsontree.h"

#include "user_settings.h"
#include "user_tcp.h"
#include "light.h"

static struct espconn *pTCPClient = NULL;
static struct espconn *pTCPServer = NULL;
static os_event_t *pTCPQueue = NULL;

static char TCPData[100];

uint32_t FTCPClientState = FTCP_CLIENT_EMPTY;
uint32_t FTCPServerState = FTCP_SERVER_EMPTY;

extern volatile uint32_t ticks;

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
InitTCPTask(void){

	pTCPQueue = (os_event_t*) os_malloc(TCP_QUEUE_LENGTH*sizeof(os_event_t));
	system_os_task(TCPTask, TCP_TASK_PRIORITY, pTCPQueue, TCP_QUEUE_LENGTH);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPTask(os_event_t *pevent){
	struct jsonparse_state state;
	int json_type;
	char string[128];
	long value;


	if(wifi_station_get_connect_status() != STATION_GOT_IP) return;
	if((FTCPServerState & FTCP_SERVER_STARTED) == 0) TCPServerInit(); else TCPServerStop();
	switch (pevent->sig) {
		case TCP_SIGNAL_PRINT:
			//os_printf("TCP_SIGNAL_PRINT received ! Ticks count: %lu\r\n", (uint32_t)pevent->par);
			os_printf("From TCPTask - Free heap size: %lu\r\n", (uint32_t)system_get_free_heap_size());
			break;
		case TCP_SIGNAL_OTHER:
			//os_printf("TCP_SIGNAL_OTHER received ! TCPSend();\r\n");
			TCPClientSend();
			break;
		case TCP_SIGNAL_SERVER_PRINT:
			//os_printf("TCP_SIGNAL_SERVER_PRINT: %s", (char*)pevent->par);
			jsonparse_setup(&state, (char*)pevent->par, strlen((char*)pevent->par));
			os_printf("pos - %d, len - %d\r\n", state.pos, state.len);
			while((json_type = jsonparse_next(&state)) != 0){
			    switch (json_type)
			    {
			      case JSON_TYPE_ARRAY:
			        os_printf("Array:\n");
			        break;

			      case JSON_TYPE_OBJECT:
			    	  os_printf("Object:\n");
			        break;

			      case JSON_TYPE_PAIR:
			    	  os_printf("Pair:\n");
			        break;

			      case JSON_TYPE_PAIR_NAME:
			    	if (jsonparse_strcmp_value(&state,"light") == 0){
			    		json_type = jsonparse_next(&state);
			    		json_type = jsonparse_next(&state);
			    		if (json_type == JSON_TYPE_NUMBER){
			    			value = jsonparse_get_value_as_long(&state);
				    		os_printf("light value => %ld\r\n", value);
				    		light_set_intencity(value);
			    		}
			    	}


			        break;

			      case JSON_TYPE_STRING:
					os_printf("String: ");
			        jsonparse_copy_value(&state, string, 128);
			        os_printf("%s\n", string);
			        break;

			      case JSON_TYPE_INT:
			    	  os_printf("Int: ");
			    	  os_printf("%d\n", jsonparse_get_value_as_int(&state));
			        break;

			      case JSON_TYPE_NUMBER:
			    	  os_printf("Number: ");
			    	  os_printf("%ld\n", jsonparse_get_value_as_long(&state));
			        break;

			      case JSON_TYPE_ERROR:
			    	  os_printf("Error:\n");
			        break;

			      default:
			        // Turns out these are the ASCII codes for non-whitespace in the JSON
			        // that is not handled as special case by the parser.
			    	  os_printf("Unknown type: %d\n", json_type);
			        break;
			        //- See more at: http://www.esp8266.com/viewtopic.php?f=9&t=9439#sthash.5UAAOpEV.dpuf			}
			    }
			}
			break;
		default:
			break;
	}
}

//=============================================================================================================================================================================================================================================

int8_t ICACHE_FLASH_ATTR
TCPClientInit(){
	int8_t result = ESPCONN_OK;

	pTCPClient = (struct espconn*)os_zalloc(sizeof(struct espconn));
	if(pTCPClient == NULL) return (ESPCONN_MEM);
	pTCPClient->type = ESPCONN_TCP;
	pTCPClient->state = ESPCONN_NONE;
	pTCPClient->proto.tcp = (esp_tcp*) os_zalloc(sizeof(esp_tcp));
	if(pTCPClient->proto.tcp == NULL){
		os_free(pTCPClient);
		os_printf("pTCPClient->proto.tcp = (esp_tcp*) os_zalloc(sizeof(esp_tcp)); - FAILS\r\n");
		return (ESPCONN_MEM);
	}
	pTCPClient->proto.tcp->local_port = espconn_port();
	pTCPClient->proto.tcp->remote_port = RemoteTCPClientPort;
	os_memcpy(pTCPClient->proto.tcp->remote_ip, RemoteIP, 4);
	espconn_regist_connectcb(pTCPClient, TCPClientConnectCallback);
	espconn_regist_sentcb(pTCPClient, TCPClientSentCallback);
	espconn_regist_recvcb(pTCPClient, TCPClientReceiveCallback);
	espconn_regist_disconcb(pTCPClient, TCPClientDisconnectCallback);
	espconn_regist_reconcb(pTCPClient, TCPClientReconnectCallback);

	return (result);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPClientSend(){
	int8_t result;
	if(pTCPClient == NULL){
		os_printf("From TCPTask - Free heap size: %lu\r\n", (uint32_t)system_get_free_heap_size());
		result = TCPClientInit();
		if(result == ESPCONN_OK) espconn_connect(pTCPClient);
		else os_printf("TCPClientInit() when pTCPClient == NULL. Error: %d\r\n", result);
		os_printf("From TCPTask - Free heap size: %lu\r\n", (uint32_t)system_get_free_heap_size());
	}else{
		if(FTCPClientState & FTCP_CLIENT_DISCONNECT){
			FTCPClientState ^= FTCP_CLIENT_DISCONNECT | FTCP_CLIENT_DELETE_CONNECTION;
			espconn_disconnect(pTCPClient);
			return;
		}
		if(FTCPClientState & FTCP_CLIENT_DELETE_CONNECTION){
			result = espconn_delete(pTCPClient);
			if(result == ESPCONN_OK){
				FTCPClientState =~FTCP_CLIENT_DELETE_CONNECTION;
				os_free(pTCPClient->proto.tcp);
				os_free(pTCPClient);
				pTCPClient = NULL;
				os_printf("From TCPTask - Free heap size: %lu\r\n", (uint32_t)system_get_free_heap_size());
			}
			os_printf("TCPClientInit() when FDeleteConnection == true. Error: %d\r\n", result);
		}
	}
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPClientConnectCallback(void *arg){
	os_printf("In TCPClientConnectCallback...pTCPConnection->state: %u\r\n", pTCPClient->state);
	os_sprintf(TCPData, "Hello from TCP ! Ticks: %lu. My name is %s\r\n", ticks, UserSettings.Name);
	espconn_set_opt(pTCPClient, ESPCONN_REUSEADDR | ESPCONN_NODELAY);
	espconn_send(pTCPClient, TCPData,os_strlen(TCPData)+1);
	os_printf("In TCPClientConnectCallback after send...pTCPConnection->state: %u\r\n", pTCPClient->state);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPClientDisconnectCallback(void *arg){
	os_printf("In TCPClientDisconnectCallback...pTCPConnection->state: %u\r\n", pTCPClient->state);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPClientSentCallback(void *arg){
	os_printf("In TCPClientSentCallback...pTCPConnection->state: %u\r\n", pTCPClient->state);
	FTCPClientState |= FTCP_CLIENT_DISCONNECT;
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPClientReceiveCallback(void *arg, char *pdata, uint16_t length){
	os_printf("In TCPClientReceiveCallback - %s\r\n", pdata);
	os_printf("\r\nIn TCPClientReceiveCallback...pTCPConnection->state: %u\r\n", pTCPClient->state);
}

//=============================================================================================================================================================================================================================================

void ICACHE_FLASH_ATTR
TCPClientReconnectCallback(void *arg, int8_t error){
	os_printf("In TCPClientReconnectCallback. Reason - %d\r\n", error);
	os_printf("pTCPClient value: %p\r\n", pTCPClient);
	os_printf("In TCPClientReconnectCallback...pTCPConnection->state: %u\r\n", pTCPClient->state);
	if(error != ESPCONN_OK){
		FTCPClientState |= FTCP_CLIENT_DISCONNECT;
	}
}

//=============================================================================================================================================================================================================================================

int8_t ICACHE_FLASH_ATTR
TCPServerInit(){
	int8_t result = ESPCONN_OK;

	pTCPServer = (struct espconn*)os_zalloc(sizeof(struct espconn));
	if(pTCPServer == NULL) return (ESPCONN_MEM);
	pTCPServer->type = ESPCONN_TCP;
	pTCPServer->state = ESPCONN_NONE;
	pTCPServer->proto.tcp = (esp_tcp*) os_zalloc(sizeof(esp_tcp));
	if(pTCPServer->proto.tcp == NULL){
		os_free(pTCPServer);
		return (ESPCONN_MEM);
	}
	pTCPServer->proto.tcp->local_port = LocalTCPServerPort;
	os_memcpy(pTCPServer->proto.tcp->remote_ip, RemoteIP, 4);
	espconn_regist_connectcb(pTCPServer, TCPServerConnectCallback);
	espconn_regist_sentcb(pTCPServer, TCPServerSentCallback);
	espconn_regist_recvcb(pTCPServer, TCPServerReceiveCallback);
	espconn_regist_disconcb(pTCPServer, TCPServerDisconnectCallback);
	espconn_regist_reconcb(pTCPServer, TCPServerReconnectCallback);
	espconn_accept(pTCPServer);
	espconn_regist_time(pTCPServer, 5, 1);		//set connection timeout to 5 seconds for single connection
	FTCPServerState |= FTCP_SERVER_STARTED;
	os_printf("TCP server started...\r\n");
	return (result);
}

//=============================================================================================================================================================================================================================================

void TCPServerStop(){
	if(FTCPServerState & FTCP_SERVER_DISCONNECT){
		FTCPServerState ^= FTCP_SERVER_DISCONNECT | FTCP_SERVER_DELETE;
		espconn_disconnect(pTCPServer);
	}
	if(FTCPServerState & FTCP_SERVER_DELETE){
		if(espconn_delete(pTCPServer) == ESPCONN_OK){
			FTCPServerState &=~(FTCP_SERVER_DELETE | FTCP_SERVER_DISCONNECT);
			os_free(pTCPServer->proto.tcp);
			os_free(pTCPServer);
		}
	}
}

//=============================================================================================================================================================================================================================================

void TCPServerConnectCallback(void *arg){
	os_printf("In TCPServerConnectCallback...\r\n");
}

//=============================================================================================================================================================================================================================================

void TCPServerDisconnectCallback(void *arg){
	os_printf("In TCPServerDisconnectCallback...\r\n");
}

//=============================================================================================================================================================================================================================================

void TCPServerSentCallback(void *arg){
	os_printf("In TCPServerSentCallback...\r\n");
}

//=============================================================================================================================================================================================================================================

void TCPServerReceiveCallback(void *arg, char *pdata, uint16_t length){
	os_printf("In TCPServerReceiveCallback...\r\n");
	system_os_post(TCP_TASK_PRIORITY, TCP_SIGNAL_SERVER_PRINT, (ETSParam)pdata);
	os_printf(pdata);
}

//=============================================================================================================================================================================================================================================

void TCPServerReconnectCallback(void *arg, int8_t error){
	os_printf("In TCPServerReconnectCallback...\r\n");
}

//=============================================================================================================================================================================================================================================
